# Smart-Grid-End-to-End
# Smart-Grid
The Smart Grid will be the power grid of the future. It will offer a two way communication flow, between consumers and
providers, to ensure energy is distributed in the most efficient way. With the advance of the internet and technology in
general, we have the ability to improve the traditional power grid and turn it into an intelligent, automated and distributed
energy delivery network that will be able to assess the state of the grid in real time and adopt an appropriate mode of
operation. A key component of an efficient Smart Grid operation will be the accurate prediction of future supply and
demand trends.




### Table of content

- [Images](https://gitlab.com/Sarang43682/smart-grid-using-machine-learning/-/tree/master/Images) = Screenshot of web app
- [Static](https://gitlab.com/Sarang43682/smart-grid-using-machine-learning/-/tree/master/Static) = Images used in web
- [Templates](https://gitlab.com/Sarang43682/smart-grid-using-machine-learning/-/tree/master/templates) = HTML files
- [Training](https://gitlab.com/Sarang43682/smart-grid-using-machine-learning/-/blob/master/Training.ipynb) = Random Forrest Model Training
- [Model](https://gitlab.com/Sarang43682/smart-grid-using-machine-learning/-/blob/master/Model.py) = Model deployment using flask

### How to run
- <code>In command prompt first fetch the location of main folder using (cd) command and then type Python Model.py</code>
